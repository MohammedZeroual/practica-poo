# Practica POO #

* Esta practica se encarga de poner unos trabajadores en una empresa, y la empresa tiene que calcular las nominas de los trabajadores. Hay diferentes trabajadores : Operarios, ingenieros y Administradores.
------------------------
* Los trabajadores cobran un sueldo. Los operarios 10€, los administradores 15€ y los ingenieros 25€.
------------------------
* La empresa tiene: NIF, nombre y direccion.
* Los trabajadores tienen: DNI, Nombre, precio Hora y horas Trabajadas.
* Las nominas tienen : ID, mes, año y total.

## UML ##

![UML](imagenes/uml.png)

* Hay una relacion n-aria y una herencia de trabajador.
