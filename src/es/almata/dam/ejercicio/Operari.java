package es.almata.dam.ejercicio;

public class Operari extends Trabajador {
	
	
	//Constructores
	public Operari(String nom, String dni, int preuHora, int horesTreballades) {
		super(nom, dni, preuHora, horesTreballades);
		
	}
	//Metodo
	public void cobrar() {
		System.out.println("\tOperari cobra : 10� a la hora");
	}
	
	//SobreEscribir
	@Override
	public String toString() {
		return super.toString()+"Operari []";
	}
	
	


}
