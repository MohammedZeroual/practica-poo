package es.almata.dam.ejercicio;


public class Run {

	public static void main(String[] args) {
		//Empresa
		Empresa empresa = new Empresa("114144A", "POO S.A", "Nueva Zela, Avenida 2");
	
		//Nominas
		Nomina n1 = new Nomina("enero",2001, 1);
		Nomina n2 = new Nomina("febrero",2001, 2);
		Nomina n3 = new Nomina("mazro",2001, 3);
		Nomina n4 = new Nomina("abril",2001, 4);
		Nomina n5 = new Nomina("mayo",2001, 5);
		Nomina n6= new Nomina("junio",2001, 6);
		Nomina n7 = new Nomina("julio",2001, 7);
		Nomina n8 = new Nomina("agosto",2001, 8);
		Nomina n9 = new Nomina("septiembre",2001, 9);
		Nomina n10 = new Nomina("octubre",2001, 10);
		Nomina n11 = new Nomina("noviembre",2001, 11);
		Nomina n12 = new Nomina("diciembre",2001, 12);
		
		
		//Trabajadores
		Administratiu administrador1 = new Administratiu("Jose Luis", "151515K", 15, 15);
		administrador1.addNomina(n1);
		administrador1.addNomina(n2);
		administrador1.addNomina(n3);
		administrador1.addNomina(n4);
		administrador1.addNomina(n5);
		administrador1.addNomina(n6);
		administrador1.addNomina(n7);
		administrador1.addNomina(n8);
		administrador1.addNomina(n9);
		administrador1.addNomina(n10);
		administrador1.addNomina(n11);
		administrador1.addNomina(n12);
		
		Administratiu administrador2 = new Administratiu("Jose Maria", "151616K", 15, 10);
		administrador2.addNomina(n1);
		administrador2.addNomina(n2);
		administrador2.addNomina(n3);
		administrador2.addNomina(n4);
		administrador2.addNomina(n5);
		administrador2.addNomina(n6);
		administrador2.addNomina(n7);
		administrador2.addNomina(n8);
		administrador2.addNomina(n9);
		administrador2.addNomina(n10);
		administrador2.addNomina(n11);
		administrador2.addNomina(n12);


		Operari operari1 = new Operari("Carla Garcia", "151417Z",10, 20);
		operari1.addNomina(n1);
		operari1.addNomina(n2);
		operari1.addNomina(n3);
		operari1.addNomina(n4);
		operari1.addNomina(n5);
		operari1.addNomina(n6);
		operari1.addNomina(n7);
		operari1.addNomina(n8);
		operari1.addNomina(n9);
		operari1.addNomina(n10);
		operari1.addNomina(n11);
		operari1.addNomina(n12);
		
		Operari operari2 = new Operari("Julia Moreno", "141112Z",10, 25);
		operari2.addNomina(n1);
		operari2.addNomina(n2);
		operari2.addNomina(n3);
		operari2.addNomina(n4);
		operari2.addNomina(n5);
		operari2.addNomina(n6);
		operari2.addNomina(n7);
		operari2.addNomina(n8);
		operari2.addNomina(n9);
		operari2.addNomina(n10);
		operari2.addNomina(n11);
		operari2.addNomina(n12);

		Enginyer enginyer1 = new Enginyer("Salazr", "181716L", 25, 35);
		
		enginyer1.addNomina(n1);
		enginyer1.addNomina(n2);
		enginyer1.addNomina(n3);
		enginyer1.addNomina(n4);
		enginyer1.addNomina(n5);
		enginyer1.addNomina(n6);
		enginyer1.addNomina(n7);
		enginyer1.addNomina(n8);
		enginyer1.addNomina(n9);
		enginyer1.addNomina(n10);
		enginyer1.addNomina(n11);
		enginyer1.addNomina(n12);
		
		Enginyer enginyer2 = new Enginyer("Silvia", "121736L", 25, 45);
		enginyer2.addNomina(n1);
		enginyer2.addNomina(n2);
		enginyer2.addNomina(n3);
		enginyer2.addNomina(n4);
		enginyer2.addNomina(n5);
		enginyer2.addNomina(n6);
		enginyer2.addNomina(n7);
		enginyer2.addNomina(n8);
		enginyer2.addNomina(n9);
		enginyer2.addNomina(n10);
		enginyer2.addNomina(n11);
		enginyer2.addNomina(n12);


		//Agregar los trabajadores a la empresa
		empresa.addTrabajador(administrador1);
		empresa.addTrabajador(administrador2);
		empresa.addTrabajador(operari1);
		empresa.addTrabajador(operari2);
		empresa.addTrabajador(enginyer1);
		empresa.addTrabajador(enginyer2);
	
		
		//Calcular la nomina de cada trabajador
		System.out.println(administrador1);
		n1.setTotal(empresa.calcularN(administrador1, n1));	
		n2.setTotal(empresa.calcularN(administrador1, n2));	
		n3.setTotal(empresa.calcularN(administrador1, n3));	
		n4.setTotal(empresa.calcularN(administrador1, n4));	
		n5.setTotal(empresa.calcularN(administrador1, n5));	
		n6.setTotal(empresa.calcularN(administrador1, n6));	
		n7.setTotal(empresa.calcularN(administrador1, n7));	
		n8.setTotal(empresa.calcularN(administrador1, n8));	
		n9.setTotal(empresa.calcularN(administrador1, n9));	
		n10.setTotal(empresa.calcularN(administrador1, n10));	
		n11.setTotal(empresa.calcularN(administrador1, n11));	
		n12.setTotal(empresa.calcularN(administrador1, n12));	
		administrador1.nominaTrabajadores(administrador1);
		System.out.println("\n\n\n");
		System.out.println(administrador2);
		n1.setTotal(empresa.calcularN(administrador2, n1));	
		n2.setTotal(empresa.calcularN(administrador2, n2));	
		n3.setTotal(empresa.calcularN(administrador2, n3));	
		n4.setTotal(empresa.calcularN(administrador2, n4));	
		n5.setTotal(empresa.calcularN(administrador2, n5));	
		n6.setTotal(empresa.calcularN(administrador2, n6));	
		n7.setTotal(empresa.calcularN(administrador2, n7));	
		n8.setTotal(empresa.calcularN(administrador2, n8));	
		n9.setTotal(empresa.calcularN(administrador2, n9));	
		n10.setTotal(empresa.calcularN(administrador2, n10));	
		n11.setTotal(empresa.calcularN(administrador2, n11));	
		n12.setTotal(empresa.calcularN(administrador2, n12));	
		administrador2.nominaTrabajadores(administrador2);
		System.out.println("\n\n\n");
		System.out.println(operari1);
		n1.setTotal(empresa.calcularN(operari1, n1));	
		n2.setTotal(empresa.calcularN(operari1, n2));	
		n3.setTotal(empresa.calcularN(operari1, n3));	
		n4.setTotal(empresa.calcularN(operari1, n4));	
		n5.setTotal(empresa.calcularN(operari1, n5));	
		n6.setTotal(empresa.calcularN(operari1, n6));	
		n7.setTotal(empresa.calcularN(operari1, n7));	
		n8.setTotal(empresa.calcularN(operari1, n8));	
		n9.setTotal(empresa.calcularN(operari1, n9));	
		n10.setTotal(empresa.calcularN(operari1, n10));	
		n11.setTotal(empresa.calcularN(operari1, n11));	
		n12.setTotal(empresa.calcularN(operari1, n12));	
		operari1.nominaTrabajadores(operari1);
		System.out.println("\n\n\n");
		System.out.println(operari2);
		n1.setTotal(empresa.calcularN(operari2, n1));	
		n2.setTotal(empresa.calcularN(operari2, n2));	
		n3.setTotal(empresa.calcularN(operari2, n3));	
		n4.setTotal(empresa.calcularN(operari2, n4));	
		n5.setTotal(empresa.calcularN(operari2, n5));	
		n6.setTotal(empresa.calcularN(operari2, n6));	
		n7.setTotal(empresa.calcularN(operari2, n7));	
		n8.setTotal(empresa.calcularN(operari2, n8));	
		n9.setTotal(empresa.calcularN(operari2, n9));	
		n10.setTotal(empresa.calcularN(operari2, n10));	
		n11.setTotal(empresa.calcularN(operari2, n11));	
		n12.setTotal(empresa.calcularN(operari2, n12));	
		operari2.nominaTrabajadores(operari2);
		System.out.println("\n\n\n");
		System.out.println(enginyer1);
		n1.setTotal(empresa.calcularN(enginyer1, n1));	
		n2.setTotal(empresa.calcularN(enginyer1, n2));	
		n3.setTotal(empresa.calcularN(enginyer1, n3));	
		n4.setTotal(empresa.calcularN(enginyer1, n4));	
		n5.setTotal(empresa.calcularN(enginyer1, n5));	
		n6.setTotal(empresa.calcularN(enginyer1, n6));	
		n7.setTotal(empresa.calcularN(enginyer1, n7));	
		n8.setTotal(empresa.calcularN(enginyer1, n8));	
		n9.setTotal(empresa.calcularN(enginyer1, n9));	
		n10.setTotal(empresa.calcularN(enginyer1, n10));	
		n11.setTotal(empresa.calcularN(enginyer1, n11));	
		n12.setTotal(empresa.calcularN(enginyer1, n12));	
		enginyer1.nominaTrabajadores(enginyer1);
		System.out.println("\n\n\n");
		System.out.println(enginyer2);
		n1.setTotal(empresa.calcularN(enginyer2, n1));	
		n2.setTotal(empresa.calcularN(enginyer2, n2));	
		n3.setTotal(empresa.calcularN(enginyer2, n3));	
		n4.setTotal(empresa.calcularN(enginyer2, n4));	
		n5.setTotal(empresa.calcularN(enginyer2, n5));	
		n6.setTotal(empresa.calcularN(enginyer2, n6));	
		n7.setTotal(empresa.calcularN(enginyer2, n7));	
		n8.setTotal(empresa.calcularN(enginyer2, n8));	
		n9.setTotal(empresa.calcularN(enginyer2, n9));	
		n10.setTotal(empresa.calcularN(enginyer2, n10));	
		n11.setTotal(empresa.calcularN(enginyer2, n11));	
		n12.setTotal(empresa.calcularN(enginyer2, n12));	
		enginyer2.nominaTrabajadores(enginyer2);

		
		//Polimorfismo
		System.out.println("\n\nCada trabajador cobra:\n");
		cobra(administrador1);
		cobra(administrador2);
		cobra(operari1);
		cobra(operari2);
		cobra(enginyer1);
		cobra(enginyer2);

		
	}
	
	public static void cobra(Trabajador trabajador) {
		trabajador.cobrar();
	}


}
