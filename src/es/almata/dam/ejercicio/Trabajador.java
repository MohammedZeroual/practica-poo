package es.almata.dam.ejercicio;

import java.util.ArrayList;

public abstract class Trabajador {
	private String nom;
	private String dni;
	private int preuHora;
	private int horesTreballades;
	private Empresa empresa;
	private ArrayList<Nomina> nominas = new ArrayList<Nomina>();
	
	public abstract void cobrar();
	
	
	//Constructores
	public Trabajador() {
		
	}

	public Trabajador(String nom, String dni, int preuHora, int horesTreballades) {
		super();
		this.nom = nom;
		this.dni = dni;
		this.preuHora = preuHora;
		this.horesTreballades = horesTreballades;
	}

	
	//Getters and Setters
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public int getPreuHora() {
		return preuHora;
	}

	public void setPreuHora(int preuHora) {
		this.preuHora = preuHora;
	}

	public int getHoresTreballades() {
		return horesTreballades;
	}

	public void setHoresTreballades(int horesTreballades) {
		this.horesTreballades = horesTreballades;
	}
	
	
	public Empresa getEmpresa() {
		return empresa;
	}


	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
	
	//Metodos  de negocio
	
	public void addNomina(Nomina n) {
		nominas.add(n);
		n.setTrabajador(this);
	}
	
	//Todas las norminas
	public void nominasEmpresa() {
		for(Nomina nomina: nominas) {
			System.out.println(nomina);
		}
	}
	// Las  nominas de un trabajador
	public void nominaTrabajadores(Trabajador trabajador) {
		for(Nomina nominas: nominas) {
			System.out.println(nominas);
		}
	}

	//SobreEscribir
	@Override
	public String toString() {
		return empresa.toString()+"\nTrabajador [nom=" + nom + ", dni=" + dni + ", preuHora=" + preuHora + ", horesTreballades="
				+ horesTreballades + "]";
	}
	
	
	
	
}
