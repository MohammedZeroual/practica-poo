package es.almata.dam.ejercicio;


public class Nomina {
	private String Mes;
	private int a�o;
	private int ID;
	private int total;
	
	private Trabajador trabajador;

	//Constructores
	public Nomina(String mes, int a�o, int iD) {
		super();
		Mes = mes;
		this.a�o = a�o;
		ID = iD;
	}
	//Getters and Setters
	public String getMes() {
		return Mes;
	}

	public void setMes(String mes) {
		Mes = mes;
	}

	public int getA�o() {
		return a�o;
	}

	public void setA�o(int a�o) {
		this.a�o = a�o;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}
	

	public Trabajador getTrabajador() {
		return trabajador;
	}
	public void setTrabajador(Trabajador trabajador) {
		this.trabajador = trabajador;
	}
	
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	//metodos de negocio 

	
	
	
	@Override
	public String toString() {
		return "Nomina [Mes=" + Mes + ", a�o=" + a�o + ", ID=" + ID + ", total=" + total + "]";
	}
	
	
	
	
}
