package es.almata.dam.ejercicio;

import java.util.ArrayList;

public class Empresa {
	private String NIF;
	private String nom;
	private String adre�a;
	
	private ArrayList<Trabajador> trabajadores = new ArrayList<Trabajador>();
	
	
	
	//Constructores
	public Empresa(String nIF, String nom, String adre�a) {
		super();
		NIF = nIF;
		this.nom = nom;
		this.adre�a = adre�a;
	}
	//Getters and Setters
	public String getNIF() {
		return NIF;
	}

	public void setNIF(String nIF) {
		NIF = nIF;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getAdre�a() {
		return adre�a;
	}

	public void setAdre�a(String adre�a) {
		this.adre�a = adre�a;
	}
	
	
	//Metodos De Negocio
	
	public void addTrabajador(Trabajador t) {
		trabajadores.add(t);
		t.setEmpresa(this);
	}
	
	public int calcularN(Trabajador t,Nomina n) {
		return t.getHoresTreballades()*t.getPreuHora();
	}
	/* Esto era un intento que apartir de las array ir al trabajador y calcular sus nominas automaticamente, no funciona.
	public void calcular(ArrayList<Trabajador> trabajadores,ArrayList<Nomina> nominas) {
		for(int i=0;i<trabajadores.size();i++) {
			System.out.println(trabajadores.get(i));
			for(int e=0;e<nominas.size();e++) {
				System.out.println(calcularN(trabajadores.get(i),nominas.get(e)));
			}
		}
	}*/
	

	
	//SobreEscribir
	@Override
	public String toString() {
		return "Empresa [NIF=" + NIF + ", nom=" + nom + ", adre�a=" + adre�a + "]";
	}
	
	
	
	

}
