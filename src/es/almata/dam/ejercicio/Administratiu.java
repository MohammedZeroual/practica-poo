package es.almata.dam.ejercicio;

public class Administratiu extends Trabajador {

	//Constructores
	public Administratiu(String nom, String dni, int preuHora, int horesTreballades) {
		super(nom, dni, preuHora, horesTreballades);
	}
	//Metodo
	public void cobrar() {
		System.out.println("\tAdministratiu cobra : 15� a la hora");
	}
	
	//SobreEscribir
	@Override
	public String toString() {
		return super.toString()+"Administratiu []";
	}

	
}
